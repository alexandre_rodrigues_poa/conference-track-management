# Conference Track Management

### Requisitos:

1. JDK 8.
2. Gradle 4.9.

---

### Executando

1. Pelo console entre na pasta do projeto e digite: gradle build
2. Após a execução do build, digite: java -jar build/libs/conference-track-management-1.0.jar
3. Agora você pode utilizar o programa.

---

### Utilizando

Comandos para utilizar o programa:

* Digite a descrição da proposta e pressione a tecla _enter_. Caso a descrição esteja no formato experado será exibida uma mensagem. Exemplo `Added 'Writing Fast Tests Against Enterprise Rails 60min' proposal`
* Para imprimir as propostas submetidas digite o comando _[print]_ e pressione a tecla _enter_ 
* Para limpar as propostas submetidas digite o comando _[clear]_ a pressione tecla _enter_
* Para sair digite o comando _[exit]_ e pressione a tecla _enter_