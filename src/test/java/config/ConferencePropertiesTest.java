package config;

import br.com.conference.config.ConferenceProperties;
import br.com.conference.domain.EventPeriod;
import org.junit.Test;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;

public class ConferencePropertiesTest {

    @Test
    public void testTimeFormatter() {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("hh:mma");
        assertEquals(timeFormatter.toString(), ConferenceProperties.getTimeFormatter().toString());
    }

    @Test
    public void testMorningSessionPeriod() {
        LocalTime start = LocalTime.of(9, 0, 0);
        LocalTime end = LocalTime.of(12, 0, 0);
        EventPeriod expected = new EventPeriod(start, end);
        assertEquals(expected, ConferenceProperties.getMorningSessionPeriod());
    }

    @Test
    public void testLunchPeriod() {
        LocalTime start = LocalTime.of(12, 0, 0);
        LocalTime end = LocalTime.of(13, 0, 0);
        EventPeriod expected = new EventPeriod(start, end);
        assertEquals(expected, ConferenceProperties.getLunchPeriod());
    }

    @Test
    public void testAfternoonSessionPeriod() {
        LocalTime start = LocalTime.of(13, 0, 0);
        LocalTime end = LocalTime.of(17, 0, 0);
        EventPeriod expected = new EventPeriod(start, end);
        assertEquals(expected, ConferenceProperties.getAfternoonPeriod());
    }

    @Test
    public void testNetworkPeriod() {
        LocalTime start = LocalTime.of(17, 0, 0);
        LocalTime end = LocalTime.of(18, 0, 0);
        EventPeriod expected = new EventPeriod(start, end);
        assertEquals(expected, ConferenceProperties.getNetworkingPeriod());
    }
}