package br.com.conference.application;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ReadingState.class})
public class ReadingStateTest {

    private @Mock ConsoleInputContext context;
    private @Mock PrintStream out;
    private final String info = "\nEnter the proposals or [print] to print the proposals, [clear] to clear the proposals and [exit] to exit:";

    @Before
    public void setUp() {
        System.setOut(out);
    }

    @Test
    public void testEmptyOrBlankLine() throws Exception {
        when(context.readLine()).thenReturn(" ");

        ConsoleInputState state = new ReadingState();
        state.execute(context);

        verify(out, times(1)).println(info);
        verify(context, times(1)).setState(state);
    }

    @Test
    public void testClearCommand() throws Exception {
        CleaningState cleaningState = mock(CleaningState.class);
        PowerMockito.whenNew(CleaningState.class).withNoArguments().thenReturn(cleaningState);
        when(context.readLine()).thenReturn("[clear]");

        ConsoleInputState state = new ReadingState();
        state.execute(context);

        verify(out, times(1)).println(info);
        verify(context, times(1)).setState(cleaningState);
    }

    @Test
    public void testPrintCommand() throws Exception {
        PrintingState printingState = mock(PrintingState.class);
        PowerMockito.whenNew(PrintingState.class).withNoArguments().thenReturn(printingState);
        when(context.readLine()).thenReturn("[print]");

        ConsoleInputState state = new ReadingState();
        state.execute(context);

        verify(out, times(1)).println(info);
        verify(context, times(1)).setState(printingState);
    }

    @Test
    public void testExitCommand() throws Exception {
        ExitingState exitingState = mock(ExitingState.class);
        PowerMockito.whenNew(ExitingState.class).withNoArguments().thenReturn(exitingState);
        when(context.readLine()).thenReturn("[exit]");

        ConsoleInputState state = new ReadingState();
        state.execute(context);

        verify(out, times(1)).println(info);
        verify(context, times(1)).setState(exitingState);
    }

    @Test
    public void testAddProposal() throws Exception {
        String proposal = "Proposal Title 45min";

        AddingState addingState = mock(AddingState.class);
        PowerMockito.whenNew(AddingState.class).withArguments(proposal).thenReturn(addingState);

        when(context.readLine()).thenReturn(proposal);

        ConsoleInputState state = new ReadingState();
        state.execute(context);

        verify(out, times(1)).println(info);
        verify(context, times(1)).setState(addingState);
    }
}