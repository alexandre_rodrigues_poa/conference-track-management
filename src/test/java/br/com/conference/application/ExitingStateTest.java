package br.com.conference.application;

import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.PrintStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ExitingStateTest {

    @Test
    public void execute() throws IOException {
        PrintStream out = mock(PrintStream.class);
        ConsoleInputContext context  = mock(ConsoleInputContext.class);
        System.setOut(out);

        ConsoleInputState state = new ExitingState();
        state.execute(context);
        verify(out, times(1)).println("Exit!");
    }
}