package br.com.conference.application;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.PrintStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CleaningState.class)
public class CleaningStateTest {

    @Test
    public void testExecute() throws Exception {
        ConsoleInputContext context = mock(ConsoleInputContext.class);

        PrintStream out = mock(PrintStream.class);
        System.setOut(out);

        ReadingState readingState = new ReadingState();
        PowerMockito.whenNew(ReadingState.class).withNoArguments().thenReturn(readingState);

        ConsoleInputState state = new CleaningState();
        state.execute(context);

        verify(context, times(1)).clearProposals();
        verify(out, times(1)).println("The proposals were cleared");
    }
}