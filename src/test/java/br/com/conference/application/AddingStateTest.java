package br.com.conference.application;

import br.com.conference.domain.Proposal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.io.PrintStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({AddingState.class})
public class AddingStateTest {

    private @Mock ConsoleInputContext context;
    private @Mock PrintStream out;

    @Test
    public void testValidProposal() throws Exception {
        String line = "Proposal Valid 45min";
        Proposal proposal = Proposal.parser(line);

        ReadingState readingState = new ReadingState();
        PowerMockito.whenNew(ReadingState.class).withNoArguments().thenReturn(readingState);

        PrintStream out = mock(PrintStream.class);
        System.setOut(out);

        ConsoleInputState state = new AddingState(line);
        state.execute(context);

        verify(context, times(1)).add(proposal);
        verify(out, times(1)).println(String.format("\nAdded '%s' proposal", proposal));
        verify(context, times(1)).setState(readingState);
    }

    @Test
    public void testInvalidProposal() throws Exception {
        String line = "Proposal invalid";
        String message = parserInvalidProposal(line);

        PrintStream out = mock(PrintStream.class);
        System.setOut(out);

        ReadingState readingState = new ReadingState();
        PowerMockito.whenNew(ReadingState.class).withNoArguments().thenReturn(readingState);

        ConsoleInputState state = new AddingState(line);
        state.execute(context);

        verify(System.out, times(1)).println(String.format("Invalid proposal: '%s'", message));
        verify(context, times(1)).setState(readingState);
    }

    @Test
    public void testNullProposal() throws Exception {
        String line = null;
        String message = parserInvalidProposal(line);

        PrintStream out = mock(PrintStream.class);
        System.setOut(out);

        ReadingState readingState = new ReadingState();
        PowerMockito.whenNew(ReadingState.class).withNoArguments().thenReturn(readingState);

        ConsoleInputState state = new AddingState(line);
        state.execute(context);

        verify(System.out, times(1)).println(String.format("Invalid proposal: '%s'", message));
        verify(context, times(1)).setState(readingState);
    }

    private String parserInvalidProposal(String line) {
        try {
            Proposal.parser(line);
            return null;
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}