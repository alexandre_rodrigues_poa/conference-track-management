package br.com.conference.application;

import br.com.conference.domain.Proposal;
import br.com.conference.domain.Tracks;
import br.com.conference.domain.TracksReport;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ConsoleInputContextTest {

    private @Mock BufferedReader reader;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testParameterReaderCanNotBeNull() {
        thrown.expectMessage("The parameter 'reader' can not be null");
        thrown.expect(NullPointerException.class);
        new ConsoleInputContext(null);
    }

    @Test
    public void testReadLine() throws IOException {
        String line  = "line 01";
        when(reader.readLine()).thenReturn(line);
        ConsoleInputContext context = new ConsoleInputContext(reader);
        assertEquals(line, context.readLine());
    }

    @Test
    public void testAddProposal() {
        String line  = "Lua for the Masses 30min";
        Proposal proposal = Proposal.parser(line);
        ConsoleInputContext context = new ConsoleInputContext(reader);
        context.add(proposal);
        assertEquals(Collections.singletonList(proposal), context.getProposals());
    }

    @Test
    public void testParameterProposalCanNotBeNull() {
        thrown.expectMessage("The parameter 'proposal' can not be null");
        thrown.expect(NullPointerException.class);
        ConsoleInputContext context = new ConsoleInputContext(reader);
        context.add(null);
    }

    @Test
    public void testClearProposal() {
        String line  = "Lua for the Masses 30min";
        Proposal proposal = Proposal.parser(line);
        ConsoleInputContext context = new ConsoleInputContext(reader);
        context.add(proposal);
        assertFalse(context.getProposals().isEmpty());
        context.clearProposals();
        assertTrue(context.getProposals().isEmpty());
    }

    @Test
    public void testPrint() {
        Proposal proposal01 = Proposal.parser("Lua for the Masses 30min");
        Proposal proposal02 = Proposal.parser("Sit Down and Write 30min");
        Tracks tracks = new Tracks(Arrays.asList(proposal01, proposal02));
        TracksReport report = new TracksReport();
        String text = report.text(tracks);

        PrintStream out = mock(PrintStream.class);
        System.setOut(out);

        ConsoleInputContext context = new ConsoleInputContext(reader);
        context.add(proposal01);
        context.add(proposal02);
        context.print();

        verify(out, times(1)).println(text);
    }

    @Test
    public void testSetState() throws IOException {
        ConsoleInputState state = mock(ConsoleInputState.class);

        ConsoleInputContext context = new ConsoleInputContext(reader);
        context.setState(state);

        verify(state, times(1)).execute(context);
    }
}