package br.com.conference.domain;

import br.com.conference.config.ConferenceProperties;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;

public class TalkTest {

    private final LocalTime time = LocalTime.now();
    private final Proposal proposal = new Proposal("Proposal Name", 45);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGetTime() {
        Talk talk = new Talk(time, proposal);
        assertEquals(time, talk.getTime());
    }

    @Test
    public void testTimeParameterCanNotBeNull() {
        thrown.expectMessage("The parameter 'time' can not be null");
        thrown.expect(NullPointerException.class);
        new Talk(null, proposal);
    }

    @Test
    public void testGetName() {
        Talk talk = new Talk(time, proposal);
        assertEquals(proposal.toString(), talk.getName());
    }

    @Test
    public void testProposalParameterCanNotBeNull() {
        thrown.expectMessage("The parameter 'proposal' can not be null");
        thrown.expect(NullPointerException.class);
        new Talk(time, null);
    }

    @Test
    public void testGetDuration() {
        Talk talk = new Talk(time, proposal);
        assertEquals(proposal.getTime(), talk.getDuration());
    }

    @Test
    public void testToString() {
        DateTimeFormatter formatter = ConferenceProperties.getTimeFormatter();
        String timeFormatted = time.format(formatter);
        String expected = "Talk{" +
                "time=" + timeFormatted +
                ", proposal=" + proposal +
                '}';

        Talk talk = new Talk(time, proposal);
        assertEquals(expected, talk.toString());
    }
}