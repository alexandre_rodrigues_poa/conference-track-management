package br.com.conference.domain;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.LocalTime;

import static org.junit.Assert.*;

public class EventAnyTest {

    private final LocalTime time = LocalTime.now();
    private final String name = "Event Test";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testParameterTimeCanNotBeNull() {
        thrown.expectMessage("The parameter 'time' can not be null");
        thrown.expect(NullPointerException.class);
        new EventAny(null, name);
    }

    @Test
    public void testParameterNameCanNotBeNull() {
        thrown.expectMessage("The parameter 'name' can not be null");
        thrown.expect(NullPointerException.class);
        new EventAny(time, null);
    }

    @Test
    public void testParameterNameCanNotBeEmptyOrBlan() {
        thrown.expectMessage("The parameter 'name' can not be empty or blank");
        thrown.expect(IllegalArgumentException.class);
        new EventAny(time, " ");
    }

    @Test
    public void testGetTime() {
        Event event = new EventAny(time, name);
        assertEquals(time, event.getTime());
    }

    @Test
    public void getName() {
        Event event = new EventAny(time, name);
        assertEquals(name, event.getName());
    }

    @Test
    public void testEquals() {
        assertEquals(new EventAny(time, name), new EventAny(time, name));
    }
}