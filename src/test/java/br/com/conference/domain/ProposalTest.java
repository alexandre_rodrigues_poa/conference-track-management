package br.com.conference.domain;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class ProposalTest {

    private final String name = "Proposal Name";
    private final int time = 45;

    private final String regexMinutes = "(?i)min$";
    private final String regexLightning = "(?i)lightning";
    private final String regexTime = "[0-9]+" + regexMinutes;
    private final String regexDescription = "^.+"+regexTime;
    private final String regexLightningDescription = "^.+(?i)"+regexLightning;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGetName() {
        Proposal proposal = new Proposal(name, time);
        assertEquals(name, proposal.getName());
    }

    @Test
    public void testNameParameterCanNotBeNull() {
        thrown.expectMessage("The parameter 'name' can not be null");
        thrown.expect(NullPointerException.class);
        new Proposal(null, time);
    }

    @Test
    public void testNameParameterCanNotBeEmpty() {
        thrown.expectMessage("The parameter 'name' can not be empty or blank");
        thrown.expect(IllegalArgumentException.class);
        new Proposal("", time);
    }

    @Test
    public void testNameParameterCanNotBeBlank() {
        thrown.expectMessage("The parameter 'name' can not be empty or blank");
        thrown.expect(IllegalArgumentException.class);
        new Proposal("  ", time);
    }

    @Test
    public void testGetTime() {
        Proposal proposal = new Proposal(name, time);
        assertEquals(time, proposal.getTime());
    }

    @Test
    public void testTimeParameterCanNotBeNegative() {
        thrown.expectMessage("The 'time' parameter can not be negative or equal to zero");
        thrown.expect(IllegalArgumentException.class);
        new Proposal(name, -10);
    }

    @Test
    public void testTimeParameterCanNotBeEqualToZero() {
        thrown.expectMessage("The 'time' parameter can not be negative or equal to zero");
        thrown.expect(IllegalArgumentException.class);
        new Proposal(name, 0);
    }

    @Test
    public void testParser() {
        final String description = "045 fasdfa 45min 125MIN";

        assertTrue(description.matches(regexDescription));

        Matcher matcher = Pattern.compile(regexTime).matcher(description);
        assertTrue(matcher.find());
        String strTime = matcher.group(0).replaceAll(regexMinutes, "");
        String name = matcher.replaceFirst("").trim();
        int time = Integer.valueOf(strTime);

        Proposal proposalExpected = new Proposal(name, time);
        Proposal proposalActual = Proposal.parser(description);

        assertEquals(proposalExpected, proposalActual);
    }

    @Test
    public void testParserLightningProposal() {
        final String description = "045 fasdfa 45min LighTninG";

        assertTrue(description.matches(regexLightningDescription));

        Matcher matcher = Pattern.compile(regexLightning).matcher(description);
        assertTrue(matcher.find());
        String name = matcher.replaceFirst("").trim();
        int time = 5;

        Proposal proposalExpected = new Proposal(name, time);
        Proposal proposalActual = Proposal.parser(description);

        assertEquals(proposalExpected, proposalActual);
    }

    @Test
    public void testParserInvalidProposal() {
        final String description = "45min fasdfa";

        assertFalse(description.matches(regexDescription));

        thrown.expectMessage(String.format("The proposal description can not be parser: '%s'", description));
        thrown.expect(IllegalArgumentException.class);

        Proposal.parser(description);
    }

    @Test
    public void testParserNullDescription() {
        thrown.expectMessage("The parameter 'description' can not null");
        thrown.expect(NullPointerException.class);
        Proposal.parser(null);
    }

    @Test
    public void testToStringTightning() {
        String expected = name + " lightning";
        Proposal proposal = new Proposal(name, 5);
        assertEquals(expected, proposal.toString());
    }
}