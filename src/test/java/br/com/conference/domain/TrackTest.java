package br.com.conference.domain;

import br.com.conference.config.ConferenceProperties;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.LocalTime;
import java.util.List;

import static org.junit.Assert.*;

public class TrackTest {

    private final String title = "Track Title";
    private final Session morningSession = new Session(ConferenceProperties.getMorningSessionPeriod());
    private final Session afternoonSession = new Session(ConferenceProperties.getAfternoonPeriod());

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testParameterTitleCanNotBeNull() {
        thrown.expectMessage("The parameter 'title' can not be null");
        thrown.expect(NullPointerException.class);
        new Track(null, morningSession, afternoonSession);
    }

    @Test
    public void testParameterTitleCanNotBeEmptyOrBlank() {
        thrown.expectMessage("The parameter 'title' can not be empty or blank");
        thrown.expect(IllegalArgumentException.class);
        new Track(" ", morningSession, afternoonSession);
    }

    @Test
    public void testGetTitle() {
        Track track = new Track(title, morningSession, afternoonSession);
        assertEquals(title, track.getTitle());
    }

    @Test
    public void testGetMorningEvents() {
        morningSession.add(new Proposal("Morning Proposal 01", 45));
        morningSession.add(new Proposal("Morning Proposal 02", 5));
        morningSession.add(new Proposal("Morning Proposal 03", 15));
        List<Event> expected = morningSession.talks();

        Track track = new Track(title, morningSession, afternoonSession);

        assertEquals(expected, track.getMorningEvents());
    }

    @Test
    public void testMorningSessionParameterCanNotBeNull() {
        thrown.expectMessage("The parameter 'morningSession' can not be null");
        thrown.expect(NullPointerException.class);
        new Track(title, null, afternoonSession);
    }

    @Test
    public void testMorningSessionPeriodIsNotValid() {
        Session morningSession = new Session(ConferenceProperties.getNetworkingPeriod());
        String mensagem = "The parameter 'morningSession' is not valid. expected '%s', actual '%s'";

        thrown.expectMessage(String.format(mensagem, ConferenceProperties.getMorningSessionPeriod(), morningSession.getSessionPeriod()));
        thrown.expect(IllegalArgumentException.class);
        new Track(title, morningSession, afternoonSession);
    }

    @Test
    public void testGetAfternoonEvents() {
        afternoonSession.add(new Proposal("Afternoon Proposal 01", 45));
        afternoonSession.add(new Proposal("Afternoon Proposal 02", 5));
        afternoonSession.add(new Proposal("Afternoon Proposal 03", 15));
        List<Event> expected = afternoonSession.talks();

        Track track = new Track(title, morningSession, afternoonSession);
        assertEquals(expected, track.getAfternoonEvents());
    }

    @Test
    public void testAfternoonSessionSessionParameterCanNotBeNull() {
        thrown.expectMessage("The parameter 'afternoonSession' can not be null");
        thrown.expect(NullPointerException.class);
        new Track(title, morningSession, null);
    }

    @Test
    public void testAfternoonSessionPeriodIsNotValid() {
        Session afternoonSession = new Session(ConferenceProperties.getMorningSessionPeriod());
        String mensagem = "The parameter 'afternoonSession' is not valid. expected '%s', actual '%s'";

        thrown.expectMessage(String.format(mensagem, ConferenceProperties.getAfternoonPeriod(), afternoonSession.getSessionPeriod()));
        thrown.expect(IllegalArgumentException.class);
        new Track(title, morningSession, afternoonSession);
    }

    @Test
    public void testGetLunchEvent() {
        LocalTime time = ConferenceProperties.getLunchPeriod().getStartTime();
        Event expected = new EventAny(time, "Lunch");

        Track track = new Track(title, morningSession, afternoonSession);
        assertEquals(expected, track.getLunchEvent());
    }

    @Test
    public void testGetNetworkingEvent() {
        LocalTime time = ConferenceProperties.getNetworkingPeriod().getStartTime();
        Event expected = new EventAny(time, "Networking Event");

        Track track = new Track(title, morningSession, afternoonSession);
        assertEquals(expected, track.getNetworkingEvent());
    }
}