package br.com.conference.domain;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.LocalTime;

import static org.junit.Assert.*;

public class EventPeriodTest {
    private final LocalTime startTime = LocalTime.of(9, 45, 35);
    private final LocalTime endTime = startTime.plusHours(2).plusMinutes(45);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testStartTime() {
        EventPeriod eventPeriod = new EventPeriod(startTime, endTime);
        assertEquals(startTime, eventPeriod.getStartTime());
    }

    @Test
    public void testStartTimeParameterCanNotBeNull() {
        thrown.expectMessage("The parameter 'startTime' can not be null");
        thrown.expect(NullPointerException.class);
        new EventPeriod(null, endTime);
    }

    @Test
    public void testEndTime() {
        EventPeriod eventPeriod = new EventPeriod(startTime, endTime);
        assertEquals(endTime, eventPeriod.getEndTime());
    }

    @Test
    public void testEndTimeParameterCanNotBeNull() {
        thrown.expectMessage("The parameter 'endTime' can not be null");
        thrown.expect(NullPointerException.class);
        new EventPeriod(startTime, null);
    }

    @Test
    public void testTheStartTimeCanNotBeAfterTheEndTime() {
        LocalTime startTime = LocalTime.of(2, 0, 0);
        LocalTime endTime = LocalTime.of(1, 45, 0);

        String message = String.format("The start time '%s' can not be after the end time '%s'", startTime, endTime);
        thrown.expectMessage(message);
        thrown.expect(IllegalArgumentException.class);

        new EventPeriod(startTime, endTime);
    }

    @Test
    public void testTheStartTimeCanNotBeEqualsToEndTime() {
        String message = String.format("The start time '%s' can not be equals to end time", startTime);
        thrown.expectMessage(message);
        thrown.expect(IllegalArgumentException.class);

        new EventPeriod(startTime, startTime);
    }

    @Test
    public void testToString() {
        String expected = "Event Period {" +
                "start=" + startTime +
                ", end=" + endTime +
                '}';
        EventPeriod eventPeriod = new EventPeriod(startTime, endTime);
        assertEquals(expected, eventPeriod.toString());
    }
}