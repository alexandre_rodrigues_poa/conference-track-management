package br.com.conference.domain;

import br.com.conference.config.ConferenceProperties;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class TracksTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testListTracks() {
        List<Proposal> proposals = inputData.stream().map(Proposal::parser).collect(Collectors.toList());

        List<Track> expected = new LinkedList<>();

        for(int i = 1; !proposals.isEmpty(); i++)
            expected.add(createTalk("Track " + i + ":",  proposals));

        proposals = inputData.stream().map(Proposal::parser).collect(Collectors.toList());
        Tracks tracks = new Tracks(proposals);
        assertEquals(expected, tracks.list());
    }

    @Test
    public void testParameterTitleCanNotBeNull() {
        thrown.expectMessage("The parameter 'proposals' can not be null");
        thrown.expect(NullPointerException.class);
        new Tracks(null);
    }

    private Track createTalk(String title, List<Proposal> proposals) {

        Session morningSession = new Session(ConferenceProperties.getMorningSessionPeriod());
        proposals.removeIf(morningSession::add);

        Session afternoonSession = new Session(ConferenceProperties.getAfternoonPeriod());
        proposals.removeIf(afternoonSession::add);

        return new Track(title, morningSession, afternoonSession);

    }

    private final List<String> inputData = Arrays.asList(
            "Writing Fast Tests Against Enterprise Rails 60min",
            "Overdoing it in Python 45min",
            "Lua for the Masses 30min",
            "Ruby Errors from Mismatched Gem Versions 45min",
            "Ruby on Rails: Why We Should Move On 60min",
            "Common Ruby Errors 45min",
            "Pair Programming vs Noise 45min",
            "Programming in the Boondocks of Seattle 30min",
            "Ruby vs. Clojure for Back-End Development 30min",
            "User Interface CSS in Rails Apps 30min",
            "Communicating Over Distance 60min",
            "Rails Magic 60min",
            "Woah 30min",
            "Sit Down and Write 30min",
            "Accounting-Driven Development 45min",
            "Clojure Ate Scala (on my project) 45min",
            "A World Without HackerNews 30min",
            "Ruby on Rails Legacy App Maintenance 60min",
            "Rails for Python Developers lightning");
}
