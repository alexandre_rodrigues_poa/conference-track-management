package br.com.conference.domain;

import br.com.conference.config.ConferenceProperties;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import static br.com.conference.config.ConferenceProperties.*;
import static java.util.stream.Collectors.*;
import static org.junit.Assert.*;

public class TracksReportTest {

    private Tracks tracks;

    @Before
    public void setUp() {
        List<Proposal> proposals = input.stream().map(Proposal::parser).collect(toList());
        tracks = new Tracks(proposals);
    }

    @Test
    public void testTextReport() {
        String expected = tracks.list().stream().map(track -> {
            String title = track.getTitle();
            Function<Event, String> toDescription = event -> getTimeFormatter().format(event.getTime()) + " " + event.getName();
            String morningEvents = track.getMorningEvents().stream().map(toDescription).collect(joining("\n"));
            String lunchEvent = Stream.of(track.getLunchEvent()).map(toDescription).collect(joining("\n"));
            String afternoonEvents = track.getAfternoonEvents().stream().map(toDescription).collect(joining("\n"));
            String networkingEvent = Stream.of(track.getNetworkingEvent()).map(toDescription).collect(joining("\n"));
            return Stream.of(title, morningEvents, lunchEvent, afternoonEvents, networkingEvent).collect(joining("\n"));
        }).collect(joining("\n\n"));

        TracksReport report = new TracksReport();
        assertEquals(expected, report.text(tracks));
    }

    private final List<String> input = Arrays.asList(
            "Writing Fast Tests Against Enterprise Rails 60min",
            "Overdoing it in Python 45min",
            "Lua for the Masses 30min",
            "Ruby Errors from Mismatched Gem Versions 45min",
            "Common Ruby Errors 45min",
            "Rails for Python Developers lightning",
            "Communicating Over Distance 60min",
            "Accounting-Driven Development 45min",
            "Woah 30min",
            "Sit Down and Write 30min",
            "Pair Programming vs Noise 45min",
            "Rails Magic 60min",
            "Ruby on Rails: Why We Should Move On 60min",
            "Clojure Ate Scala (on my project) 45min",
            "Programming in the Boondocks of Seattle 30min",
            "Ruby vs. Clojure for Back-End Development 30min",
            "Ruby on Rails Legacy App Maintenance 60min",
            "A World Without HackerNews 30min",
            "User Interface CSS in Rails Apps 30min");
}