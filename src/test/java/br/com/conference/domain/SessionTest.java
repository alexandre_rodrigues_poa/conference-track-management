package br.com.conference.domain;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class SessionTest {

    private final LocalTime startTime = LocalTime.of(9, 45, 35);
    private final LocalTime endTime = startTime.plusHours(2).plusMinutes(45);
    private final EventPeriod sessionPeriod = new EventPeriod(startTime, endTime);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testStartTime() {
        Session session = new Session(sessionPeriod);
        assertEquals(sessionPeriod, session.getSessionPeriod());
    }

    @Test
    public void testStartTimeParameterCanNotBeNull() {
        thrown.expectMessage("The parameter 'sessionPeriod' can not be null");
        thrown.expect(NullPointerException.class);
        new Session(null);
    }

    @Test
    public void testAddProposal() {
        Proposal proposal01 = new Proposal("Proposal 01", 45);
        Proposal proposal02 = new Proposal("Proposal 02", 15);
        Proposal proposal03 = new Proposal("Proposal 03", 30);
        List<Proposal> proposals = Arrays.asList(proposal01, proposal02, proposal03);

        List<Event> talks = new ArrayList<>();

        LocalTime time = startTime;
        for(Proposal proposal : proposals) {
            Talk talk = new Talk(time, proposal);
            talks.add(talk);
            time = time.plusMinutes(talk.getDuration());
        }

        Session session = new Session(sessionPeriod);
        proposals.forEach(session::add);

        assertEquals(talks, session.talks());
    }

    @Test
    public void testAddIfHasSpace() {
        final LocalTime startTime = LocalTime.of(8, 0, 0);
        final LocalTime endTime = startTime.plusHours(1).plusMinutes(35);
        final EventPeriod sessionPeriod = new EventPeriod(startTime, endTime);

        Proposal proposal01 = new Proposal("Proposal 01", 45);
        Proposal proposal02 = new Proposal("Proposal 02", 15);
        Proposal proposal03 = new Proposal("Proposal 03", 45);
        Proposal proposal04 = new Proposal("Proposal 04", 30);
        List<Proposal> proposals = Arrays.asList(proposal01, proposal02, proposal03, proposal04);

        final long sessionSpace = ChronoUnit.MINUTES.between(startTime, endTime);

        List<Event> talks = new ArrayList<>();

        long actualSpace = 0;
        for(Proposal proposal : proposals) {
            long requiredSpace = actualSpace + proposal.getTime();
            if (requiredSpace <= sessionSpace) {
                Talk talk = new Talk(startTime.plusMinutes(actualSpace), proposal);
                talks.add(talk);
                actualSpace = requiredSpace;
            }
        }

        Session session = new Session(sessionPeriod);
        proposals.forEach(session::add);
        assertEquals(talks, session.talks());
    }

    @Test
    public void testReturnTrueIfAdd() {
        final LocalTime startTime = LocalTime.of(8, 0, 0);
        final LocalTime endTime = startTime.plusMinutes(30);
        final EventPeriod sessionPeriod = new EventPeriod(startTime, endTime);

        Proposal proposal01 = new Proposal("Proposal 01", 15);
        Proposal proposal02 = new Proposal("Proposal 02", 30);
        Proposal proposal03 = new Proposal("Proposal 03", 5);

        Session session = new Session(sessionPeriod);
        assertTrue(session.add(proposal01));
        assertFalse(session.add(proposal02));
        assertTrue(session.add(proposal03));
    }

    @Test
    public void testProposalParameterCanNotBeNull() {
        thrown.expectMessage("The parameter 'proposal' can not be null");
        thrown.expect(NullPointerException.class);
        Session session = new Session(sessionPeriod);
        session.add(null);
    }
}