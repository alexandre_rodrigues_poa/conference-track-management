package br.com.conference.domain;

import java.util.List;
import java.util.Objects;

import static br.com.conference.config.ConferenceProperties.*;

public class Track {

    private final String title;
    private final Session morningSession;
    private final Event lunchEvent;
    private final Session afternoonSession;
    private final Event networkingEvent;

    Track(String title, Session morningSession, Session afternoonSession) {
        validateParameters(title, morningSession, afternoonSession);

        this.title = title;
        this.morningSession = morningSession;
        this.lunchEvent = new EventAny(getLunchPeriod().getStartTime(), "Lunch");
        this.afternoonSession = afternoonSession;
        this.networkingEvent = new EventAny(getNetworkingPeriod().getStartTime(), "Networking Event");
    }

    private void validateParameters(String title, Session morningSession, Session afternoonSession) {
        Objects.requireNonNull(title, "The parameter 'title' can not be null");
        Objects.requireNonNull(morningSession, "The parameter 'morningSession' can not be null");
        Objects.requireNonNull(afternoonSession, "The parameter 'afternoonSession' can not be null");

        if (title.trim().isEmpty())
            throw new IllegalArgumentException("The parameter 'title' can not be empty or blank");

        if (sessionPeriodInvalid(morningSession, getMorningSessionPeriod())) {
            String mensagem = invalidPeriodMessage("morningSession", getMorningSessionPeriod(), morningSession);
            throw new IllegalArgumentException(mensagem);
        }

        if (sessionPeriodInvalid(afternoonSession, getAfternoonPeriod())) {
            String mensagem = invalidPeriodMessage("afternoonSession", getAfternoonPeriod(), afternoonSession);
            throw new IllegalArgumentException(mensagem);
        }
    }

    private boolean sessionPeriodInvalid(Session session, EventPeriod sessionPeriod) {
        return !session.getSessionPeriod().equals(sessionPeriod);
    }

    private String invalidPeriodMessage(String parameterName, EventPeriod expectedPeriod,  Session session) {
        String mensagem = "The parameter '%s' is not valid. expected '%s', actual '%s'";
        return  String.format(mensagem, parameterName, expectedPeriod, session.getSessionPeriod());
    }

    String getTitle() {
        return title;
    }


    List<Event> getMorningEvents() {
        return morningSession.talks();
    }

    List<Event> getAfternoonEvents() {
        return afternoonSession.talks();
    }

    Event getLunchEvent() {
        return lunchEvent;
    }

    Event getNetworkingEvent() {
        return networkingEvent;
    }

    @Override
    public String toString() {
        return "Track{" +
                "title='" + title + '\'' +
                ", morningSession=" + morningSession +
                ", lunchEvent='" + lunchEvent + '\'' +
                ", afternoonSession=" + afternoonSession +
                ", networkingEvent='" + networkingEvent + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Track track = (Track) o;
        return Objects.equals(title, track.title) &&
                Objects.equals(morningSession, track.morningSession) &&
                Objects.equals(lunchEvent, track.lunchEvent) &&
                Objects.equals(afternoonSession, track.afternoonSession) &&
                Objects.equals(networkingEvent, track.networkingEvent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, morningSession, lunchEvent, afternoonSession, networkingEvent);
    }
}
