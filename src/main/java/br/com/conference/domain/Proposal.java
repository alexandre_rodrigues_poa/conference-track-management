package br.com.conference.domain;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Proposal {

    private static final String REGEX_MINUTES = "(?i)min$";
    private static final String REGEX_LIGHTNING = "(?i)lightning$";
    private static final String REGEX_TIME = "[0-9]+" + REGEX_MINUTES;
    private static final String REGEX_DESCRIPTION = "^.+" + REGEX_TIME;
    private static final String REGEX_LIGHTNING_DESCRIPTION = "^.+" + REGEX_LIGHTNING;

    private static final Pattern PATTERN_TIME = Pattern.compile(REGEX_TIME);
    private static final Pattern PATTERN_LIGHTNING = Pattern.compile(REGEX_LIGHTNING);
    private static final int TIME_LIGHTNING = 5;

    private String name;
    private int time;

    Proposal(String name, int time) {
        Objects.requireNonNull(name, "The parameter 'name' can not be null");
        if (name.trim().isEmpty())
            throw new IllegalArgumentException("The parameter 'name' can not be empty or blank");
        if (time <= 0)
            throw new IllegalArgumentException("The 'time' parameter can not be negative or equal to zero");

        this.name = name;
        this.time = time;
    }

    String getName() {
        return name;
    }

    int getTime() {
        return time;
    }

    public static Proposal parser(String description) {
        Objects.requireNonNull(description, "The parameter 'description' can not null");

        if (!(description.matches(REGEX_DESCRIPTION) || description.matches(REGEX_LIGHTNING_DESCRIPTION)))
            throw new IllegalArgumentException(invalidDescription(description));

        Matcher matcher = PATTERN_TIME.matcher(description);
        if (matcher.find()) {
            String name = parserName(matcher);
            String timeDescription = matcher.group().replaceFirst(REGEX_MINUTES, "");
            int time = Integer.valueOf(timeDescription);

            return new Proposal(name, time);
        }

        matcher = PATTERN_LIGHTNING.matcher(description);
        if (matcher.find()) {
            String name = parserName(matcher);
            return new Proposal(name, TIME_LIGHTNING);
        }

        throw new IllegalArgumentException(invalidDescription(description));
    }

    private static String invalidDescription(String description) {
        return String.format("The proposal description can not be parser: '%s'", description);
    }

    private static String parserName(Matcher matcher) {
        return matcher.replaceFirst("").trim();
    }

    @Override
    public String toString() {
        String timeDescription = time == TIME_LIGHTNING ? "lightning" : time + "min";
        return name + " " + timeDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proposal proposal = (Proposal) o;
        return time == proposal.time &&
                Objects.equals(name, proposal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, time);
    }
}
