package br.com.conference.domain;

import java.time.LocalTime;
import java.util.Objects;

public class EventPeriod {

    private final LocalTime endTime;
    private LocalTime startTime;

    public EventPeriod(LocalTime startTime, LocalTime endTime) {
        Objects.requireNonNull(startTime, "The parameter 'startTime' can not be null");
        Objects.requireNonNull(endTime, "The parameter 'endTime' can not be null");

        if (startTime.isAfter(endTime)) {
            String message = String.format("The start time '%s' can not be after the end time '%s'", startTime, endTime);
            throw new IllegalArgumentException(message);
        }

        if (startTime.equals(endTime)) {
            String message = String.format("The start time '%s' can not be equals to end time", startTime);
            throw new IllegalArgumentException(message);
        }

        this.startTime = startTime;
        this.endTime = endTime;
    }

    LocalTime getStartTime() {
        return this.startTime;
    }

    LocalTime getEndTime() {
        return this.endTime;
    }

    @Override
    public String toString() {
        return "Event Period {" +
                "start=" + startTime +
                ", end=" + endTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventPeriod that = (EventPeriod) o;
        return Objects.equals(endTime, that.endTime) &&
                Objects.equals(startTime, that.startTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(endTime, startTime);
    }
}
