package br.com.conference.domain;

import java.time.LocalTime;

interface Event {
    LocalTime getTime();
    String getName();
}
