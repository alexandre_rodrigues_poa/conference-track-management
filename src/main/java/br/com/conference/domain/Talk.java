package br.com.conference.domain;

import br.com.conference.config.ConferenceProperties;

import java.time.LocalTime;
import java.util.Objects;

public class Talk implements Event {

    private LocalTime time;
    private Proposal proposal;

    Talk(LocalTime time, Proposal proposal) {
        this.time = Objects.requireNonNull(time, "The parameter 'time' can not be null");
        this.proposal = Objects.requireNonNull(proposal, "The parameter 'proposal' can not be null");
    }

    @Override
    public LocalTime getTime() {
        return time;
    }

    @Override
    public String getName() {
        return proposal.toString();
    }

    int getDuration() {
        return proposal.getTime();
    }

    @Override
    public String toString() {
        return "Talk{" +
                "time=" + time.format(ConferenceProperties.getTimeFormatter()) +
                ", proposal=" + proposal +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;
        Talk talk = (Talk) o;
        return Objects.equals(time, talk.time) &&
                Objects.equals(proposal, talk.proposal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time, proposal);
    }
}
