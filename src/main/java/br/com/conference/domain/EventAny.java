package br.com.conference.domain;

import java.time.LocalTime;
import java.util.Objects;

public class EventAny implements Event {

    private LocalTime time;
    private String name;

    EventAny(LocalTime time, String name) {
        Objects.requireNonNull(time, "The parameter 'time' can not be null");
        Objects.requireNonNull(name, "The parameter 'name' can not be null");
        if (name.trim().isEmpty())
            throw new IllegalArgumentException("The parameter 'name' can not be empty or blank");
        this.time = time;
        this.name = name;
    }

    @Override
    public LocalTime getTime() {
        return this.time;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return "EventAny{" +
                "time=" + time +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;
        EventAny eventAny = (EventAny) o;
        return Objects.equals(time, eventAny.time) &&
                Objects.equals(name, eventAny.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time, name);
    }
}
