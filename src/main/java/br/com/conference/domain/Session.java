package br.com.conference.domain;

import java.time.temporal.ChronoUnit;
import java.util.*;

public class Session {
    private EventPeriod sessionPeriod;
    private long sessionSpace;
    private long actualSpace;
    private List<Talk> talks = new LinkedList<>();

    Session(EventPeriod sessionPeriod) {
        Objects.requireNonNull(sessionPeriod, "The parameter 'sessionPeriod' can not be null");

        this.sessionPeriod = sessionPeriod;
        this.actualSpace = 0;
        this.sessionSpace = ChronoUnit.MINUTES.between(sessionPeriod.getStartTime(), sessionPeriod.getEndTime());
    }

    EventPeriod getSessionPeriod() {
        return sessionPeriod;
    }

    boolean add(Proposal proposal) {
        Objects.requireNonNull(proposal, "The parameter 'proposal' can not be null");

        long requiredSpace = actualSpace + proposal.getTime();
        if (requiredSpace <= sessionSpace) {
            Talk talk = new Talk(sessionPeriod.getStartTime().plusMinutes(actualSpace), proposal);
            talks.add(talk);
            actualSpace = requiredSpace;
            return true;
        }
        return false;
    }

    List<Event> talks() {
        return Collections.unmodifiableList(talks);
    }

    @Override
    public String toString() {
        return "Session{" + sessionPeriod +
                ", talks=" + talks +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return Objects.equals(sessionPeriod, session.sessionPeriod) &&
                Objects.equals(talks, session.talks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionPeriod, talks);
    }
}
