package br.com.conference.domain;

import br.com.conference.config.ConferenceProperties;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Tracks {

    private final List<Track> tracks = new LinkedList<>();

    public Tracks(List<Proposal> proposals) {
        Objects.requireNonNull(proposals, "The parameter 'proposals' can not be null");

        proposals = new LinkedList<>(proposals);
        for(int i = 1; !proposals.isEmpty(); i++) {
            String title = String.format("Track %d:", i);
            tracks.add(createTrack(title, proposals));
        }

    }

    private Track createTrack(String title, List<Proposal> proposals) {
        Session morningSession = createSession(ConferenceProperties.getMorningSessionPeriod(), proposals);
        Session afternoonSession = createSession(ConferenceProperties.getAfternoonPeriod(), proposals);
        return new Track(title, morningSession, afternoonSession);
    }

    private Session createSession(EventPeriod sessionPeriod, List<Proposal> proposals) {
        Session session = new Session(sessionPeriod);
        proposals.removeIf(session::add);
        return session;
    }

    List<Track> list() {
        return Collections.unmodifiableList(tracks);
    }

    @Override
    public String toString() {
        return "Tracks{" + tracks + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tracks tracks1 = (Tracks) o;
        return Objects.equals(tracks, tracks1.tracks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tracks);
    }
}
