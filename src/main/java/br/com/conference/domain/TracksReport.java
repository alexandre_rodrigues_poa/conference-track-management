package br.com.conference.domain;

import java.util.function.Function;
import java.util.stream.Stream;

import static br.com.conference.config.ConferenceProperties.getTimeFormatter;
import static java.util.stream.Collectors.joining;

public class TracksReport {

    public String text(Tracks tracks) {
        Stream<Track> stream = tracks.list().stream();
        return stream.map(toText()).collect(joining("\n\n"));
    }

    private Function<Track, String> toText() {
        return track -> {
            String title = track.getTitle();
            String morningEvents = toDescription(track.getMorningEvents().stream());
            String lunchEvent = toDescription(Stream.of(track.getLunchEvent()));
            String afternoonEvents = toDescription(track.getAfternoonEvents().stream());
            String networkingEvent = toDescription(Stream.of(track.getNetworkingEvent()));
            return String.join("\n", title, morningEvents, lunchEvent, afternoonEvents, networkingEvent);
        };
    }

    private String toDescription(Stream<Event> stream) {
        return stream.map(toDescription()).collect(joining("\n"));
    }

    private Function<Event, String> toDescription() {
        return event -> getTimeFormatter().format(event.getTime()) + " " + event.getName();
    }
}
