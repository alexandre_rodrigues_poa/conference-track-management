package br.com.conference.application;

import java.io.IOException;

public class PrintingState implements ConsoleInputState {

    @Override
    public void execute(ConsoleInputContext context) throws IOException {
        context.print();
        context.setState(new ReadingState());
    }
}
