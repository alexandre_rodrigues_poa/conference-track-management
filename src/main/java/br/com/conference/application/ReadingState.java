package br.com.conference.application;

import java.io.IOException;

public class ReadingState implements ConsoleInputState {

    @Override
    public void execute(ConsoleInputContext context) throws IOException {
        System.out.println("\nEnter the proposals or [print] to print the proposals, [clear] to clear the proposals and [exit] to exit:");

        String line = context.readLine().trim();
        if (line.isEmpty())
            context.setState(this);
        else if (line.equals("[clear]"))
            context.setState(new CleaningState());
        else if (line.equals("[exit]"))
            context.setState(new ExitingState());
        else if (line.equals("[print]"))
            context.setState(new PrintingState());
        else
            context.setState(new AddingState(line));
    }
}
