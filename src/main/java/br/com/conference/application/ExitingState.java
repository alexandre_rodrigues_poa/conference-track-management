package br.com.conference.application;

import java.io.IOException;

public class ExitingState implements ConsoleInputState {

    @Override
    public void execute(ConsoleInputContext context) {
        System.out.println("Exit!");
    }
}
