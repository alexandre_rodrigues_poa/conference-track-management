package br.com.conference.application;

import br.com.conference.domain.Proposal;
import br.com.conference.domain.Tracks;
import br.com.conference.domain.TracksReport;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

class ConsoleInputContext {
    private final BufferedReader reader;
    private List<Proposal> proposals = new LinkedList<>();

    ConsoleInputContext(BufferedReader reader) {
        this.reader = Objects.requireNonNull(reader, "The parameter 'reader' can not be null");
    }

    String readLine() throws IOException {
        return reader.readLine();
    }

    void add(Proposal proposal) {
        proposals.add(Objects.requireNonNull(proposal, "The parameter 'proposal' can not be null"));
    }

    List<Proposal> getProposals() {
        return Collections.unmodifiableList(proposals);
    }

    void clearProposals() {
        proposals.clear();
    }

    void print() {
        Tracks tracks = new Tracks(proposals);
        TracksReport report = new TracksReport();
        System.out.println(report.text(tracks));
    }

    void setState(ConsoleInputState state) throws IOException {
        state.execute(this);
    }

    void start() throws IOException {
        setState(new ReadingState());
    }

    @Override
    public String toString() {
        return "ConsoleInputContext{" +
                "reader=" + reader +
                ", proposals=" + proposals +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConsoleInputContext context = (ConsoleInputContext) o;
        return Objects.equals(reader, context.reader) &&
                Objects.equals(proposals, context.proposals);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reader, proposals);
    }
}
