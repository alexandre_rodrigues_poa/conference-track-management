package br.com.conference.application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConferenceApplication {

    public static void main (String[] args) throws IOException {

        System.out.println("***********************************");
        System.out.println("**** CONFERENCE TRACK MANAGENT ****");
        System.out.println("***********************************");


        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));) {
            ConsoleInputContext context = new ConsoleInputContext(reader);
            context.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
