package br.com.conference.application;

import br.com.conference.domain.Proposal;

import java.io.IOException;

public class AddingState implements ConsoleInputState {

    private final String line;

    AddingState(String line) {
        this.line = line;
    }

    @Override
    public void execute(ConsoleInputContext context) throws IOException {
        addProposal(context);
        context.setState(new ReadingState());
    }

    private void addProposal(ConsoleInputContext context) {
        try {
            Proposal proposal = Proposal.parser(line);
            context.add(proposal);
            System.out.println(String.format("\nAdded '%s' proposal", proposal));
        } catch (IllegalArgumentException | NullPointerException e) {
            System.out.println(String.format("Invalid proposal: '%s'", e.getMessage()));
        }
    }
}
