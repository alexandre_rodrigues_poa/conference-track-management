package br.com.conference.application;

import java.io.IOException;

public interface ConsoleInputState {
    void execute(ConsoleInputContext context) throws IOException;
}
