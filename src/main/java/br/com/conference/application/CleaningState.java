package br.com.conference.application;

import java.io.IOException;

public class CleaningState implements ConsoleInputState {

    @Override
    public void execute(ConsoleInputContext context) throws IOException {
        context.clearProposals();
        System.out.println("The proposals were cleared");
        context.setState(new ReadingState());
    }
}
