package br.com.conference.config;

import br.com.conference.domain.EventPeriod;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class ConferenceProperties {

    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("hh:mma");

    public static EventPeriod getMorningSessionPeriod() {
        LocalTime start = LocalTime.of(9, 0, 0);
        LocalTime end = LocalTime.of(12, 0, 0);
        return new EventPeriod(start, end);
    }

    public static EventPeriod getLunchPeriod() {
        LocalTime start = LocalTime.of(12, 0, 0);
        LocalTime end = LocalTime.of(13, 0, 0);
        return new EventPeriod(start, end);
    }

    public static EventPeriod getAfternoonPeriod() {
        LocalTime start = LocalTime.of(13, 0, 0);
        LocalTime end = LocalTime.of(17, 0, 0);
        return new EventPeriod(start, end);
    }

    public static EventPeriod getNetworkingPeriod() {
        LocalTime start = LocalTime.of(17, 0, 0);
        LocalTime end = LocalTime.of(18, 0, 0);
        return new EventPeriod(start, end);
    }

    public static DateTimeFormatter getTimeFormatter() {
        return TIME_FORMATTER;
    }
}
